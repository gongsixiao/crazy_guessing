var aldstat = require("./utils/ald-stat.js");
const open = require('./utils/openId.js')
App({
  onLaunch: function () {
    open.Openidromise().then(res=>{
      let openid = res.data.openid
      this.globalData.openid = openid
    })
  },
  globalData: {
    userInfo: null,
    openid:''
  }
})
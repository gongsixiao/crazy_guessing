
const app = getApp();
const aldstat = require("../../utils/ald-stat.js");
Page({
  data: {
    avatar:'',
    gold:'',
    nickname:'',
    redbag:'',
    serviceTap: false,//客服弹窗
    guRule:false,//点击金币
    userId: '',
    Result:[]
  },
  onShow:function(){
    this.personalCenter()
  },
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中'
    })
    //获取跳转小程序ID和路径
    wx.request({
      url: 'https://www.qiff.club/index.php/index/member/bindingProgram',
      method: 'POST',
      header: {
        "content-type": "application/x-www-form-urlencoded"
      },
      success: res => {
        var data = res.data.data[1]
        this.setData({
          Gappid: data.appid,
          Gpath: data.path
        })
      }
    })
    this.personalCenter()
    this.getSetting()
    this.template()
  },
  template:function(){
    wx.request({
      url: 'https://www.qiff.club/index.php/index/customer/bettingRedbag',
      method: 'POST',
      header: {
        "content-type": "application/x-www-form-urlencoded"
      },
      data: {
        openid: app.globalData.openid
      },
      success: res => {
        let data = [];
        res.data.data.forEach((item,index)=>{
          if(index>=3){
            return false
          }else{
            data.push(item)
          }
        })
        this.setData({
          Result: data
        })
      },
      complete: function () {
        wx.hideLoading()
      }
    })
  },
  personalCenter:function(){//获取个人数据
    wx.request({
      url: 'https://www.qiff.club/index.php/index/customer/personalCenter',
      method: 'POST',
      header: {
        "content-type": "application/x-www-form-urlencoded"
      },
      data: {
        openid: app.globalData.openid
      },
      success: res => {
        //console.log(res)
        this.setData({
          avatar: res.data.data.avatar,
          gold: res.data.data.gold,
          nickname: res.data.data.nickname,
          redbag: res.data.data.redbag,
          userId:res.data.data.id
        })
      },
      complete: function () {
        wx.hideLoading()
      }
    })
  },
  moneyBtn:function(){
    this.setData({
      guRule: true
    })
  },
  guruleH: function () {//关闭分享好友遮罩层
    this.setData({
      guRule: false
    })
  },
  goldBtn:function(){//点击弹出遮罩
    this.setData({
      guRule: true
    })
  },
  // service: function () {//点击在线客服
  //   this.setData({
  //     serviceTap: true
  //   })
  // },
  serviceTap: function () {//点击在下客服里面图片
    this.setData({
      serviceTap: false
    })
  },
  bindCancel: function () {//跳转小程序、
    wx.navigateToMiniProgram({
      appId: this.data.Gappid,
      path: this.data.Gpath
    })
    app.aldstat.sendEvent('个人中心跳转小程序');
  },
  moneyBtn:function(){//点击提现
  // 零钱不满10元不能提现
    if (Number(this.data.redbag)>=10){
      this.setData({
        serviceTap: true
      })
    }else{
      wx.showToast({
        title: '亲~10元以上才能提现哦~再接再厉~加油!!!',
        icon: 'none',
        duration: 4000
      })
    }
    
  },
  getSetting: function () {//获取用户基本信息
    wx.getUserInfo({
      success: res => {
        this.setData({
          userInfo: res.userInfo
        })
      }
    })
  },
  vsCode:function(){//点击生成专属二维码
    wx.request({
      url: 'https://www.qiff.club/index.php/index/customer/makeCode',
      method: 'POST',
      header: {
        "content-type": "application/x-www-form-urlencoded"
      },
      data: {
        openid: app.globalData.openid,
        page:"pages/start/start"
      },
      success:res=>{
        console.log(res)
        wx.previewImage({
          urls: [res.data]
        })
      }
    })
    // wx.previewImage({
    //   urls: ['https://www.qiff.club/guessing/xiaofangVSCODE.jpg']
    // })
  },
  onShareAppMessage: function (res) {//分享按钮
    var openid = app.globalData.openid
    //console.log(openid+"分享出去的")
    if (res.from === 'button') {
      // 来自页面内转发按钮
      this.setData({
        drillTapA: false,
        drillTapB: false
      })
    }
    return {
      title: this.data.userInfo.nickName + '@你，参与世界杯竞猜，和我一起瓜分万元大奖！！',
      path: '/pages/start/start?id=' + openid,
      imageUrl: 'https://www.qiff.club/guessing/onShareAppMessage.png'
    }
  }
})
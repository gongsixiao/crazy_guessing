
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    Result:[],
    rID:1
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中',
    })
    this.setData({
      rID: this.options.id
    })
    if (this.options.id!=1){
      wx.request({
        url: 'https://www.qiff.club/index.php/index/customer/bettingRecord',
        method: 'POST',
        header: {
          "content-type": "application/x-www-form-urlencoded"
        },
        data: {
          openid: app.globalData.openid
        },
        success: res => {
          this.setData({
            Result: res.data.data
          })
        },
        complete: function () {
          wx.hideLoading()
        }
      })
    }else{
      wx.request({
        url: 'https://www.qiff.club/index.php/index/customer/bettingRedbag',
        method: 'POST',
        header: {
          "content-type": "application/x-www-form-urlencoded"
        },
        data: {
          openid: app.globalData.openid
        },
        success: res => {
          this.setData({
            Result: res.data.data
          })
        },
        complete: function () {
          wx.hideLoading()
        }
      })
    }
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})
const app = getApp()
const open = require('../../utils/openId.js')
Page({
  data: {
    excode:0,
    exList: [
      { url: "https://www.qiff.club/guessing/ex/1.jpg" },
      { url: "https://www.qiff.club/guessing/ex/2.jpg" },
      { url: "https://www.qiff.club/guessing/ex/3.jpg" },
      { url: "https://www.qiff.club/guessing/ex/4.jpg" },
      { url: "https://www.qiff.club/guessing/ex/5.jpg" },
      { url: "https://www.qiff.club/guessing/ex/6.jpg" }
    ]
  },
  onLoad: function (e) {

    wx.request({
      url: 'https://www.qiff.club/index.php/index/customer/edition',
      method: 'POST',
      header: {
        "content-type": "application/x-www-form-urlencoded"
      },
      success: res => {
        //console.log(res)
        this.setData({
          excode:res.data.data.id
        })
      }
    })
    wx.setStorage({
      key:'shareID',
      data: this.options.id
    })
    wx.getSetting({
      success: res => {
        res.authSetting['scope.userInfo'] = false
        if (res.authSetting['scope.userInfo']) {
          this.getUserInfo();
          
        }
      }
    })
  },
  getUserInfo:function(){
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {//授权成功跳转首页
          //console.log('跳转到首页')
          this.switchTab('/pages/index/index')
        }
        else {
          wx.showToast({
            title: '请先进行授权',
            icon: 'none',
            duration: 2000
          })
        }
      }
    })
  },
  switchTab:function(pages){//封装跳转
    wx.switchTab({
      url: pages
    })
  }
})
const app = getApp()
const open = require('../../utils/openId.js')
const RecordData = require('../../utils/data.js')
const backgroundAudioManager = wx.getBackgroundAudioManager()
const aldstat = require("../../utils/ald-stat.js");
Page({
  data: {
    userInfo:{},//用户基本信息
    gold:'',//竞猜币
    height:"",//页面高度
    exRule:false,//竞猜规则
    guRule:false,//金币不足分享好友
    matchData:[],//今日赛事
    array:[],//选择倍数
    Record:[],//竞猜记录
    total:'',//提现人数
    scroll:true,
    current:0,//当前场次
    swiperBtn:true,//是否显示左右滑动按钮
    paused:true,//音乐暂停
    guRulea:false
  },
  onShow:function(){
    this.UserGold()
  },
  onHide:function(){
    backgroundAudioManager.pause()
  },
  onLoad: function () {
    //初始化音乐播放
    backgroundAudioManager.src = 'https://www.qiff.club/guessing/Live-It-Up.mp3'
    backgroundAudioManager.title = 'Live-It-Up'
  
    wx.showLoading({
      title: '加载中',
    })
    //获取跳转小程序ID和路径
    wx.request({
      url: 'https://www.qiff.club/index.php/index/member/bindingProgram',
      method: 'POST',
      header: {
        "content-type": "application/x-www-form-urlencoded"
      },
      success: res => {
        //console.log(res)
        var data = res.data.data[1]
        this.setData({
          Gappid: data.appid,
          Gpath: data.path
        })
      }
    })
    this.todayMatch();
    this.getSetting();
    this.withdrawDepositPerson()
    this.setData({
      Record: RecordData.data
    })
    var _this = this
    open.Openidromise().then(res=>{//封装全局获取openid
      let openid = res.data.openid
      this.setData({
        openid: openid
      })
      this.UserGold()
      //console.log('promise方法')
    })
    .then(function(){//判断用户是否存在
      wx.request({
        url: 'https://www.qiff.club/index.php/index/customer/isfirstLogin',
        method: 'POST',
        header: {
          "content-type": "application/x-www-form-urlencoded"
        },
        data: {
          openid: _this.data.openid
        },
        success: res => {
          //console.log(res.data.data.isFirstLogin)
          let isinfo = res.data.data.isFirstLogin;
          if (isinfo==1){//新用户提交基本信息
            //console.log('新用户')
            _this.NewUserInfo()
          }else{//老用户提交openid
            //console.log('老用户')
            _this.OldUserInfo()
          }
        }
      })
    })
  },
  withdrawDepositPerson:function(){//提现人数
    wx.request({
      url: 'https://www.qiff.club/index.php/index/customer/withdrawDepositPerson',
      method: 'POST',
      header: {
        "content-type": "application/x-www-form-urlencoded"
      },
      success: res => {
        //console.log(res)
        this.setData({
          total: res.data.data.total
        })
      }
    })
  },
  share: function () {//分享获得金币
    //console.log('分享得金币')
    wx.getStorage({
      key: 'shareID',
      success: res => {
        //console.log('有参数' + res.data)
        if (res.data !== '' && res.data != undefined && res.data) {
          //console.log(res.data)
          wx.request({
            url: 'https://www.qiff.club/index.php/index/customer/shareReward',
            data: {
              openid: res.data
            },
            method: 'POST',
            header: {
              "content-type": "application/x-www-form-urlencoded"
            },
            success: res => {
              wx.removeStorage({
                key:'shareID',
                success:res=>{
                  //console.log('成功调取分享金币接口，删除缓存成功')
                }
              })
            }
          })
        }
      },
      fail:res=>{
        //console.log('没有成功获取到缓存')
      }
    })
  },
  todayMatch:function(){//今日赛事
    wx.request({
      url: 'https://www.qiff.club/index.php/index/customer/todayMatch',
      method: 'POST',
      header: {
        "content-type": "application/x-www-form-urlencoded"
      },
      success:res=>{
        //console.log(res.data.data)
        this.setData({
          matchData: res.data.data
        })
        this.data.matchData <= 1 ? this.data.swiperBtn = false : this.data.swiperBtn=true
        //console.log(this.data.matchData)
      }
    })
  },
  NewUserInfo:function(){//新用户提交用户信息
    const _this = this
    wx.request({
      url: 'https://www.qiff.club/index.php/index/customer/getUserInfo',
      method: 'POST',
      header: {
        "content-type": "application/x-www-form-urlencoded"
      },
      data: {
        openid: _this.data.openid,
        nickname: _this.data.userInfo.nickName,
        gender: _this.data.userInfo.gender,
        city: _this.data.userInfo.city,
        province: _this.data.userInfo.province,
        avatar:_this.data.userInfo.avatarUrl
      },
      success: res=>{
        //console.log('提交用户信息成功'+_this.data.openid)
        this.UserGold();
        this.share()
      }
    })
  },
  OldUserInfo:function(){//老用户提交openid
    const _this = this
    wx.request({
      url: 'https://www.qiff.club/index.php/index/customer/loginReward',
      method: 'POST',
      header: {
        "content-type": "application/x-www-form-urlencoded"
      },
      data: {
        openid: _this.data.openid
      },
      success: function () {
        //console.log('老用户' + _this.data.openid)
        _this.UserGold();
      }
    })
  },
  UserGold:function(){//获取用户金币
    const _this = this
    wx.request({
      url: 'https://www.qiff.club/index.php/index/customer/getGold',
      method: 'POST',
      header: {
        "content-type": "application/x-www-form-urlencoded"
      },
      data: {
        openid: _this.data.openid
      },
      success: res => {
        res.data.data.gold == false ? res.data.data.gold = '0' : res.data.data.gold
        _this.setData({
          gold: res.data.data.gold
        })
        //console.log(res)
        //console.log('我的竞猜币',_this.data.gold)
        let arrayData = []
        for (let i = 1; i <= _this.data.gold;i++){
          arrayData.push(i)
        }
        _this.setData({
          array: arrayData
        })
      },
      complete:function(){
        wx.hideLoading()
      }
    })
  },
  bindPickerChange:function(e){//竞猜
   
    let ID = e.target.dataset.isid;//当前场次ID
    let Tap = e.target.dataset.text;//当前选中队伍胜负
    let openid = this.data.openid;//openID
    let value = Number(e.detail.value)+1;//当前选中倍数
    //console.log('当前选中倍数'+value)
    wx.request({
      url: 'https://www.qiff.club/index.php/index/customer/betting',
      data: {
        openid: openid,
        matchid: ID,
        betting: Tap,
        multiple: value
      },
      method: 'POST',
      header: {
        "content-type": "application/x-www-form-urlencoded"
      },
      success: res => {
        //console.log(res)
        if(res.data.code==0){
          this.UserGold()
          wx.showToast({
            title: '竞猜成功',
            icon: 'none',
            duration: 2000
          })
        }else{
          wx.showToast({
            title: res.data.desc,
            icon: 'none',
            duration: 2000
          })
        }
      }
    })
  },
  audioPlay:function(){//控制音乐播放
    
    if (this.data.paused){
      backgroundAudioManager.pause()
      this.setData({
        paused:false
      })
    }else{
      backgroundAudioManager.play()
      this.setData({
        paused: true
      })
    }
  },
  getSetting:function(){//获取用户基本信息
    wx.getUserInfo({
      success:res=>{
        this.setData({
          userInfo: res.userInfo
        })
      }
    })
  },
  guruleHa: function () {//关闭分享好友遮罩层
    this.setData({
      guRulea: false
    })
    console.log(111)
  },
  goldBtn: function () {//点击弹出遮罩
    this.setData({
      guRulea: true
    })
  },
  swiperRight:function(){//右滑
    this.data.current < this.data.matchData.length-1 ? this.data.current = this.data.current + 1 : this.data.current;
    this.setData({
      current: this.data.current
    })
    console.log(this.data.current)
  },
  swiperLeft:function(){//左滑
    this.data.current != 0 ? this.data.current = this.data.current - 1 : this.data.current;
    this.setData({
      current: this.data.current
    })
    console.log(this.data.current)
  },
  unexBtn:function(){//没有金币了
    this.setData({
      guRule: true
    })
  },
  guruleH:function(){//关闭分享好友遮罩层
    this.setData({
      guRule: false
    })
  },
  gushare:function(){
    this.onShareAppMessage()
  },
  exRuleClose: function () {//关闭活动规则
    this.setData({
      exRule: false
    })
  },
  exRuleBtn: function () {//首页活动规则
    this.setData({
      exRule: true
    })
  },
  bindCancel: function () {//跳转小程序、
    wx.navigateToMiniProgram({
      appId: this.data.Gappid,
      path: this.data.Gpath
    })
    app.aldstat.sendEvent('首页跳转小程序');
  },
  forwardA: function (e) {//分享-1
    e.form = 'button';
    this.onShareAppMessage(e);
    app.aldstat.sendEvent('分享-1');
  },
  forwardB: function (e) {//分享-2
    e.form = 'button';
    this.onShareAppMessage(e);
    app.aldstat.sendEvent('分享-2');
  },
  forwardC: function (e) {//分享-3
    e.form = 'button';
    this.onShareAppMessage(e);
    app.aldstat.sendEvent('分享-3');
  },
  onShareAppMessage: function (res) {//分享按钮
    var openid = app.globalData.openid;
    if (res.from === 'button') {
      // 来自页面内转发按钮
      this.setData({
        drillTapA: false,
        drillTapB: false,
        guRule:false
      })
    }
    return {
      title: this.data.userInfo.nickName +'@你，世界杯万元红包，点击领取。',
      path: '/pages/start/start?id=' + openid,
      imageUrl: 'https://www.qiff.club/guessing/onShareAppMessage.png'
    }
  }
})
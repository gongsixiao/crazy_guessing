;function Openidromise(){//封装全局获取openid
  let promise = new Promise((resolve, reject)=>{
    wx.login({
      success:code=>{
        wx.request({
          url: 'https://www.qiff.club/index.php/index/customer/getopenid',
          data: {code:code.code},
          method: 'POST',
          header: {
            "content-type": "application/x-www-form-urlencoded"
          },
          success: res => {
            resolve(res.data)
          }
        })
      }
    })
  })
  return promise
};
function PostPromise(url,data){//封装全局POST请求
  let promise = new Promise((resolve, reject)=>{
    wx.request({
      url: url,
      data:data,
      method: 'POST',
      header: {
        "content-type": "application/x-www-form-urlencoded"
      },
      success:function(res){
        resolve(res)
      }
    })
  })
  return promise
};
module.exports = {
  Openidromise: Openidromise,
  PostPromise: PostPromise
}